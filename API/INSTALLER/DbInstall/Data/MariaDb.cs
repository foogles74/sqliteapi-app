﻿using Abstraction;
using System;
using System.IO;
using System.Reflection;

namespace DbInstall.Data
{
    class MariaDb
    {
        public static bool LoadDriver(IDatabaseSetting settings)
        {
            bool result = false;
            try
            {
                IDatabase? database = null;
                byte[] data = File.ReadAllBytes(
                    Utils.GetAssemblyPath("MariaDB")
                    );
                Assembly driver = Assembly.Load(data);
                foreach (Type type in driver.GetExportedTypes())
                {
                    if (type.Name == "Database")
                    {
                        object[] param = new object[1];
                        param[0] = settings;
                        object? instance = Activator.CreateInstance(type, param);
                        if (instance != null)
                        {
                            database = (IDatabase)instance;
                        }

                    }
                }
            }
            catch { Exception e; }
            return result;
        }
    }
}


