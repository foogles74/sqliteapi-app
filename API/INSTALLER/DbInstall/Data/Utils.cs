﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace DbInstall.Data
{
    class Utils
    {
        internal static string GetAssemblyPath(string NameDb)
        {
            string? result = null;
            string path = Path.GetFullPath(Path.Combine(Assembly.GetExecutingAssembly().Location, @"..\..\..\..\..\..\"));
            result = path + $"DRIVER\\{NameDb}\\obj\\Debug\\netcoreapp3.1\\{NameDb}.dll";
            return result;
        }
    }
}
