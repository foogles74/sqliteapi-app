﻿using Abstraction;
using System;
using System.IO;
using System.Reflection;


namespace DbInstall.Data
{
    class SqLite
    {
        private static Abstraction.IDatabase? database = null;
        public static bool LoadDriver(IDatabaseSetting setting)
        {
            bool result = false;
            string drivername = "";

            try
            {
                drivername = Utils.GetAssemblyPath("SqLite");
            }
            catch (Exception)
            {
                ;
            }
            if (File.Exists(drivername))
            {
                try
                {
                    byte[] data = File.ReadAllBytes(drivername);
                    Assembly driver = Assembly.Load(data);
                    foreach (Type type in driver.GetExportedTypes())
                    {
                        if (type.Name == "Database")
                        {
                            object[] param = new object[1];
                            param[0] = (DatabaseSettings)setting;
                            object? instance = Activator.CreateInstance(type, param);
                            if (instance != null)
                            {
                                database = (IDatabase)instance;
                            }

                        }
                    }
                }
                catch (Exception e)
                {
                    ;
                }
            }
            return result;
        }


    }
}
