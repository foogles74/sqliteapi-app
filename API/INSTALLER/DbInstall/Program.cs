﻿using DbInstall.Data;
using System;

namespace DbInstall
{
    class Program
    {
        private static Abstraction.IDatabaseSetting setting =
            new DatabaseSettings();
        static void Main(string[] args)
        {
            string typeDb = null;
            Console.WriteLine("Установщик запущен!");
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-log")
                {
                    setting.Login = args[i + 1];
                }
                if (args[i] == "-loc")
                {
                    setting.Location = args[i + 1];
                }
                if (args[i] == "-p")
                {
                    setting.Password = args[i + 1];
                }
                if (args[i] == "-d")
                {
                    setting.DB = args[i + 1];
                }
                if (args[i] == "-n")
                {
                    typeDb = args[i + 1];
                }
            }
            if (typeDb == "SqLite")
            {
                if (SqLite.LoadDriver(setting)) { Console.WriteLine("Nice"); }
            }
            if (typeDb == "MariaDb")
            {
                if (MariaDb.LoadDriver(setting)) { Console.WriteLine("Nice"); }
            }
        }
    }
}
