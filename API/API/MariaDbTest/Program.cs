﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Abstraction;
using Abstraction.Entity;

namespace MariaDbTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("MariaDB test APP!");
            IDatabaseSetting settings = new DatabaseSettings();
            settings.Location = "localhost";
            settings.DB = "api";
            settings.Login = "api";
            settings.Password = "api";
            try
            {
                IDatabase? database = null;
                byte[] data = File.ReadAllBytes(
                    GetAssemblyPath()
                    );
                Assembly driver = Assembly.Load(data);
                foreach (Type type in driver.GetExportedTypes())
                {
                    if (type.Name == "Database")
                    {
                        object[] param = new object[1];
                        param[0] = settings;
                        object? instance = Activator.CreateInstance(type, param);
                        if (instance != null)
                        {
                            database = (IDatabase)instance;
                        }

                    }
                }
                if (database != null)
                {
                    Console.WriteLine("Success!");
                    Client client = new Client();
                    client.pasport = "a";
                    client.phone = "a";
                    client.polise = "a";
                    client.FIO = "a";

                    //int Postid = database.Client.Post(client);
                    //int Postid = database.Client.Put(1,client);
                    //int de = database.Client.Delete(2);
                    List<IClient> list = database.Client.Get();
                    foreach (Client item in list)
                    {
                        Console.WriteLine("Clients_id " + item.clients_id + "\nPasport " + item.pasport + "\nPolise " + item.polise + "\nPhone " + item.phone);
                    }

                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: { ex.Message }");
            }
        }
        private static string GetAssemblyPath()
        {
            string? result = null;
            string path = Path.GetFullPath(Path.Combine(Assembly.GetExecutingAssembly().Location, @"..\..\..\..\..\..\"));
            result = path + @"DRIVER\MariaDB\bin\Debug\netcoreapp3.1\MariaDB.dll";
            return result;
        }
    }
}

