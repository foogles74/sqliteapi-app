﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Vid_ObjectController : ControllerBase
    {
        // GET: api/<Vid_ObjectController>
        [HttpGet]
        public IEnumerable<IVid_object> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Vid_Object.Get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<Vid_ObjectController>/5
        [HttpGet("{id}")]
        public IVid_object Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Vid_Object.Get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<Vid_ObjectController>
        [HttpPost]
        public int Post([FromBody] Vid_object value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Vid_Object.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<Vid_ObjectController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Vid_object value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Vid_Object.Put(id,value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<Vid_ObjectController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                try {return Repository.Instance.Vid_Object.Delete(id); } catch { return 0; }
                
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}
