﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DocumentController : ControllerBase
    {
        // GET: api/<DocumentController>
        [HttpGet]
        public IEnumerable<IDocument> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Document.Get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<DocumentController>/5
        [HttpGet("{id}")]
        public IDocument Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Document.Get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<DocumentController>
        [HttpPost]
        public int Post([FromBody] Document value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Document.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<DocumentController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Document value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Document.Put(id,value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<DocumentController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                try { return Repository.Instance.Document.Delete(id); } catch { return 0; }
               
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}
