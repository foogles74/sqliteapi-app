﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Object_VidController : ControllerBase
    {
        // GET: api/<Object_VidController>
        [HttpGet]
        public IEnumerable<IObject_vid> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Object_Vid.Get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<Object_VidController>/5
        [HttpGet("{id}")]
        public IObject_vid Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Object_Vid.Get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<Object_VidController>
        [HttpPost]
        public int Post([FromBody] Object_vid value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Object_Vid.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<Object_VidController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Object_vid value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Object_Vid.Put(id,value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<Object_VidController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                try {return Repository.Instance.Object_Vid.Delete(id); } catch { return 0; }
                
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}
