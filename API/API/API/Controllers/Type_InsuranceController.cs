﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Type_InsuranceController : ControllerBase
    {
        // GET: api/<Type_IsuranceController>
        [HttpGet]
        public IEnumerable<IType_Insurance> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Type_Insurance.Get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<Type_IsuranceController>/5
        [HttpGet("{id}")]
        public IType_Insurance Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Type_Insurance.Get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<Type_IsuranceController>
        [HttpPost]
        public int Post([FromBody] Type_Insurance value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Type_Insurance.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<Type_IsuranceController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Type_Insurance value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Type_Insurance.Put(id,value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<Type_IsuranceController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                try {return Repository.Instance.Type_Insurance.Delete(id); } catch { return 0; }
                
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}
