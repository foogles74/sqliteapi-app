﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Vid_insuranceController : ControllerBase
    {
        // GET: api/<Vid_insuranceController>
        [HttpGet]
        public IEnumerable<IVid_Insurance> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Vid_Insurance.Get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<Vid_insuranceController>/5
        [HttpGet("{id}")]
        public IVid_Insurance Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Vid_Insurance.Get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<Vid_insuranceController>
        [HttpPost]
        public int Post([FromBody] Vid_Insurance value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Vid_Insurance.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<Vid_insuranceController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Vid_Insurance value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Vid_Insurance.Put(id,value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<Vid_insuranceController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                try {return Repository.Instance.Vid_Insurance.Delete(id); } catch { return 0; }
                
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}
