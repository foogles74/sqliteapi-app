﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Object_InsuranceController : ControllerBase
    {
        // GET: api/<Object_InsuranceController>
        [HttpGet]
        public IEnumerable<IObject_Insurance> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Object_Insurance.Get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<Object_InsuranceController>/5
        [HttpGet("{id}")]
        public IObject_Insurance Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Object_Insurance.Get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<Object_InsuranceController>
        [HttpPost]
        public int Post([FromBody] Object_Insurance value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Object_Insurance.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<Object_InsuranceController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Object_Insurance value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Object_Insurance.Put(id,value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<Object_InsuranceController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                try {return Repository.Instance.Object_Insurance.Delete(id); } catch { return 0; }
                
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}
