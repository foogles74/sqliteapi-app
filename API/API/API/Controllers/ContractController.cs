﻿using Microsoft.AspNetCore.Mvc;
using Abstraction.Entity;
using System.Collections.Generic;
using API.Data;
using System;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContractController : ControllerBase
    {
        // GET: api/<ContractController>
        [HttpGet]
        public IEnumerable<IContract> Get()
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Contract.Get();
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // GET api/<ContractController>/5
        [HttpGet("{id}")]
        public IContract Get(int id)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Contract.Get(id);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // POST api/<ContractController>
        [HttpPost]
        public int Post([FromBody] Contract value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Contract.Post(value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // PUT api/<ContractController>/5
        [HttpPut("{id}")]
        public int Put(int id, [FromBody] Contract value)
        {
            if (Repository.IsLoad)
            {
                return Repository.Instance.Contract.Put(id,value);
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }

        // DELETE api/<ContractController>/5
        [HttpDelete("{id}")]
        public int Delete(int id)
        {
            if (Repository.IsLoad)
            {
                try {return Repository.Instance.Contract.Delete(id); } catch { return 0; }
                
            }
            else
            {
                throw new Exception("base didnt load");
            }
        }
    }
}
