﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Abstract
{
    abstract public class DatabaseContract : Abstraction.Database.IDatabaseContract
    {
        public abstract IContract CreateOne();
        public abstract int Delete(int id);
        public abstract List<IContract> Get();
        public abstract IContract Get(int id);
        public abstract int Post(IContract value);
        public abstract int Put(int id, IContract value);
    }
}
