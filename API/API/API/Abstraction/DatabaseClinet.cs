﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Abstract
{
    abstract class DatabaseClinet : Abstraction.Database.IDatabaseClient
    {
        public abstract IClient CreateOne();
        public abstract int Delete(int id);
        public abstract List<IClient> Get();
        public abstract IClient Get(int id);
        public abstract int Post(IClient value);
        public abstract int Put(int id, IClient value);
    }
}
