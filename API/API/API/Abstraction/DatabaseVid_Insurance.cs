﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Abstract
{
    abstract public class DatabaseVid_Insurance : Abstraction.Database.IDatabaseVid_Insurance
    {
        public abstract IVid_Insurance CreateOne();
        public abstract int Delete(int id);
        public abstract List<IVid_Insurance> Get();
        public abstract IVid_Insurance Get(int id);
        public abstract int Post(IVid_Insurance value);
        public abstract int Put(int id, IVid_Insurance value);
    }
}
