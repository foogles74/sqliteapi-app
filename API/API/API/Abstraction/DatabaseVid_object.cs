﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Abstract
{
    abstract public class DatabaseVid_object : Abstraction.Database.IDatabaseVid_object
    {
        public abstract IVid_object CreateOne();
        public abstract int Delete(int id);
        public abstract List<IVid_object> Get();
        public abstract IVid_object Get(int id);
        public abstract int Post(IVid_object value);
        public abstract int Put(int id, IVid_object value);
    }
}
