﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Abstract
{
    abstract public class DatabaseObject_vid : Abstraction.Database.IDatabaseObject_vid
    {
        public abstract IObject_vid CreateOne();
        public abstract int Delete(int id);
        public abstract List<IObject_vid> Get();
        public abstract IObject_vid Get(int id);
        public abstract int Post(IObject_vid value);
        public abstract int Put(int id, IObject_vid value);
    }
}
