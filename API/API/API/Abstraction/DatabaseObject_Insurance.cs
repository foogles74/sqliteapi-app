﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Abstract
{
    abstract public class DatabaseObject_Insurance : Abstraction.Database.IDatabaseObject_Insurance
    {
        public abstract IObject_Insurance CreateOne();
        public abstract int Delete(int id);
        public abstract List<IObject_Insurance> Get();
        public abstract IObject_Insurance Get(int id);
        public abstract int Post(IObject_Insurance value);
        public abstract int Put(int id, IObject_Insurance value);
    }
}
