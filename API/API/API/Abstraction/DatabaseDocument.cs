﻿using Abstraction.Database;
using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Abstract
{
    abstract public class DatabaseDocument : Abstraction.Database.IDatabaseDocument
    {
        public abstract IDocument CreateOne();
        public abstract int Delete(int id);
        public abstract List<IDocument> Get();
        public abstract IDocument Get(int id);
        public abstract int Post(IDocument value);
        public abstract int Put(int id, IDocument value);
    }
}
