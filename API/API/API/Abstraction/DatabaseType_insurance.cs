﻿using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Abstract
{
    abstract public class DatabaseType_insurance : Abstraction.Database.IDatabaseType_insurance
    {
        public abstract IType_Insurance CreateOne();
        public abstract int Delete(int id);
        public abstract List<IType_Insurance> Get();
        public abstract IType_Insurance Get(int id);
        public abstract int Post(IType_Insurance value);
        public abstract int Put(int id, IType_Insurance value);
    }
}
