﻿using Abstraction.Database;
using Abstraction.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Abstract
{
    abstract public class DatabaseUsers : Abstraction.Database.IDatabaseUsers
    {
        public abstract IUsers GetByUserName(string userName);
        public abstract IUsers Get(int id);

    }
}
