﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Data
{
    public class DatabaseSettings : Abstraction.IDatabaseSetting
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Location { get; set; }
        public string DB { get; set; }
        public DatabaseSettings()
        {
            Location = "";
            DB = "";
            Login = "";
            Password = "";
        }

    }
}
