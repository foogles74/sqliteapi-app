﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Abstraction;
using Abstraction.Entity;

#nullable enable
namespace TestSqLite
{
    internal class Program
    {
        static void Main(string[] args)
        { 
            Console.WriteLine("SqLite test APP!");
            IDatabaseSetting settings = new DatabaseSettings();
            settings.Location = "test.db";
            try
            {
                IDatabase database = null;
                string path = GetPath();
                if (path != null)
                {
                    byte[] data = File.ReadAllBytes(path);
                    Assembly driver = Assembly.Load(data);
                    foreach (Type type in driver.GetExportedTypes())
                    {
                        if (type.Name == "Database")
                        {
                            object[] param = new object[1];
                            param[0] = settings;
                            object? instance = Activator.CreateInstance(type, param);
                            if (instance != null)
                            {
                                database = database = (IDatabase)instance;

                            }
                        }
                    }
                    if (database != null)
                    {
                        IClient client = database.Client.CreateOne();
                        client.pasport = "dasdda";
                        client.phone = "dgada";
                        client.polise = "ddaa";

                        IContract contract = database.Contract.CreateOne();
                        contract.date_end = DateTime.Now;
                        contract.date_start = DateTime.Now;
                        contract.sum_insurance = 10;
                        contract.type_Insurance_id = 1;
                        contract.client_id = 1;

                        Users dsa =  database.Users.Get(0);
                        Users dsaw1 = database.Users.GetByUserName("bebra");
                        //database.Contract.Post(contract);
                        //List<IContract> contracts = database.Contract.Get();
                        //foreach (IContract item in contracts) 
                        //{
                        //    Console.WriteLine(item.Date_end + "\n"+ item.Date_start+ "\n" + item.Contract_id + "\n" + item.Type_Insurance_id + "\n" +  item.sum_insurance);
                        //}

                        //int Deleteid = database.Client.Delete(3);
                        //int Postid = database.Client.Post(client);
                        //database.Client.Put(1, client);
                        //IClient client1 = database.Client.Get(0);
                        List<IClient> list = database.Client.Get();
                        foreach (IClient item in list)
                        {
                            Console.WriteLine("Clients_id " + item.clients_id + "\nPasport " + item.pasport + "\nPolise " + item.polise + "\nPhone " + item.phone);
                        }
                    }
                }
                else 
                {
                    new Exception("Путь не найден");
                }


                
                            
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: { ex.Message }");
            }

        }
        private static string GetPath() 
        {
            string? result = null;
            string path = Path.GetFullPath(Path.Combine(Assembly.GetExecutingAssembly().Location, @"..\..\..\..\..\..\"));
            result = path + @"DRIVER\SqLIte\obj\Debug\netcoreapp3.1\SqLIte.dll";
            return result;
        }
    }
}
