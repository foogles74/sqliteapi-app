﻿using Abstraction;
using Abstraction.Database;
using Abstraction.Entity;
using Data;
using MariaDB.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MariaDB
{
    public class Database : IDatabase
    {
        public IDatabaseClient Client => new DatabaseClient();

        public IDatabaseContract Contract => new DatabaseContract();

        public IDatabaseObject_Insurance Object_Insurance => new DatabaseObject_Insurance();

        public IDatabaseDocument Document => new DatabaseDocument();

        public IDatabaseObject_vid Object_Vid => new DatabaseObject_vid();

        public IDatabaseType_insurance Type_Insurance => new DatabaseType_insurance();

        public IDatabaseVid_Insurance Vid_Insurance => new DatabaseVid_Insurance();

        public IDatabaseVid_object Vid_Object => new DatabaseVid_object();

        public IDatabaseUsers Users => new DatabaseUsers();
        public Database(IDatabaseSetting settings)
        {
            if (DAO.Connect(settings))
            {
                if (CheckDatabase())
                {
                }
                else
                {
                    throw new Exception("Database verification failed!");
                }
            }
            else
            {
                throw new Exception("Wrong settings!");
            }
        }
        private bool CheckDatabase()
        {
            bool result = true;
            string[] tabels = DAO.Instance.GetforCheck("show tables;");
            int i = 0;
            foreach (KeyValuePair<string, string> item in DatabaseDDL.Tables)
            {
                if (tabels.Contains(item.Key))
                {
                    i += 1;
                }
                else 
                {
                    DAO.Instance.ExecuteNonQuery(item.Value);
                }
            }
            Console.WriteLine(i);
            return result;
        }
    }

}

