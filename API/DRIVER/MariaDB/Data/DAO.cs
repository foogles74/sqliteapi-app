﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using MySql.Data;
using System.Reflection;
using System.Linq;

#nullable enable
namespace MariaDB
{
    internal class DAO
    {
        private MySqlConnection connection;
        private static DAO? instance = null;
        public static DAO Instance
        {
            get
            {
                if (instance == null)
                {
                    throw (new Exception("Settrings not set!"));
                }
                else
                {
                    return instance;
                }
            }
        }
        public static bool isConnection()
        {
            if (instance != null)
            {
                return true;
            }
            return false;
        }
        public static bool Connect(Abstraction.IDatabaseSetting settings)
        {
            bool result = false;
            instance = null;
            try
            {
                instance = new DAO(settings);
                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }
            return result;
        }
        private DAO(Abstraction.IDatabaseSetting settings)
        {
            connection = new MySqlConnection();
            connection.ConnectionString = $"Server={settings.Location};Port=3306;Database={settings.DB};Uid={settings.Login};Pwd={settings.Password};";
            connection.Open();
            connection.Close();
        }
        public List<T> GetALL<T>(string query) where T : class, new()
        {
            List<T> result = new List<T>();
            try
            {
                MySqlCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = query;
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(
                        FillFromRow<T>(ref reader)
                    );
                }
                connection.Close();
            }
            catch
            {
                throw new Exception("Failed at GetAll<T>");
            }
            return result;
        }
        private T FillFromRow<T>(ref MySqlDataReader reader) where T : class, new()
        {
            T result = new T();
            PropertyInfo[] properties = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            int cnt = reader.FieldCount;
            for (int i = 0; i < cnt; i++)
            {
                string fieldName = reader.GetName(i).ToLower();
                foreach (PropertyInfo property in properties)
                {
                    if (property.Name.ToLower() == fieldName)
                    {
                        try
                        {
                            if (property.PropertyType.Name == "DateTime")
                            {
                                property.SetValue(result, Convert.ToDateTime(reader.GetValue(i)));
                                break;
                            }
                            else
                            {
                                property.SetValue(result, reader.GetValue(i));
                                break;
                            }

                        }
                        catch
                        {
                            break;
                        }

                    }
                }
            }
            return result;
        }
        public object? GetSingle(string query)
        {
            object? result = null;
            try
            {
                MySqlCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = query;
                result = command.ExecuteScalar();
                connection.Close();
            }
            catch (Exception ex)
            {
                connection.Close();
                throw ex;
            }
            return result;
        }
        public int? ExecuteNonQuery(string query)
        {
            int? result = null;
            try
            {
                MySqlCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = query;
                result = command.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception)
            {
                result = null;
                throw new Exception("Failed at ExecuteNonQuery");
            }
            return result;
        }
        public List<T> GetALL<T>(string query, List<MySqlParameter> parameters) where T : class, new()
        {
            List<T> result = new List<T>();
            try
            {

                MySqlCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = query;
                if (parameters.Count > 0)
                {
                    command.Parameters.AddRange(parameters.ToArray());
                }
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(
                        FillFromRow<T>(ref reader)
                    );
                }
                connection.Close();

            }
            catch
            {
                throw new Exception("Failed at GetAll<T>");
            }
            return result;
        }
        public string[]? GetforCheck(string query)
        {
            string[]? result = new string[] { };
            try
            {
                MySqlCommand command = connection.CreateCommand();
                connection.Open();
                command.CommandText = query;
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    //object value = reader.GetValue(0);
                    result = result.Append(reader.GetValue(0).ToString()).ToArray();
                }                
                connection.Close();
            }
            catch
            {
                throw new Exception("Failed at GetAll<T>");
            }
            return result;
        }
    }
}

