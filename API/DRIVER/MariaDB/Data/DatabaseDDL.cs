﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace MariaDB.Data
{
    class DatabaseDDL
    {
        public const string Client = "CREATE TABLE Client (Clients_id INTEGER  AUTO_INCREMENT NOT NULL, Phone VARCHAR (100) NOT NULL, Pasport VARCHAR (100) NOT NULL, Polise VARCHAR (100) NOT NULL, FIO VARCHAR (100) NOT NULL, PRIMARY KEY(Clients_id));";
        public const string Contract = "CREATE TABLE Contract (Contract_id INTEGER AUTO_INCREMENT NOT NULL, Clients_id INTEGER NOT NULL, Date_start DATE NOT NULL, Date_end DATE NOT NULL, sum_insurance DOUBLE NOT NULL, Type_Insurance_id INTEGER NOT NULL, Name VARCHAR (100) NOT NULL, PRIMARY KEY(Contract_id), CONSTRAINT `fk_Contract_Type_Insurance_id` FOREIGN KEY (Type_Insurance_id) REFERENCES Type_Insurance (Type_Insurance_id), CONSTRAINT `fk_Contract_Client_id` FOREIGN KEY (Clients_id) REFERENCES Client (Clients_id));";
        public const string Document = "CREATE TABLE Document (Document_id INTEGER AUTO_INCREMENT NOT NULL, Contract_id INTEGER NOT NULL, Object_Insurance_id INTEGER NOT NULL, PRIMARY KEY(Document_id), CONSTRAINT `fk_Document_Contract_id` FOREIGN KEY (Contract_id) REFERENCES Contract (Contract_id), CONSTRAINT `fk_Document_Object_Insurance_id` FOREIGN KEY (Object_Insurance_id) REFERENCES Object_Insurance (Object_Insurance_id));";
        public const string Object_Insurance = "CREATE TABLE Object_Insurance (Object_Insurance_id INTEGER AUTO_INCREMENT NOT NULL, Discripton VARCHAR (100), Price DOUBLE NOT NULL, Vid_object_id INTEGER NOT NULL, PRIMARY KEY(Object_Insurance_id), CONSTRAINT `fk_Object_Insurance_Vid_object_id` FOREIGN KEY (Vid_object_id) REFERENCES Vid_object (Vid_object_id));";
        public const string Object_vid = "CREATE TABLE Object_vid (Object_vid_id INTEGER AUTO_INCREMENT NOT NULL, Vid_insurance_id INTEGER NOT NULL, Vid_object_id INTEGER NOT NULL, PRIMARY KEY(Object_vid_id), CONSTRAINT `fk_Object_vid_Vid_Insurance` FOREIGN KEY (Vid_Insurance_id) REFERENCES Vid_Insurance (Vid_insurance_id), CONSTRAINT `fk_Object_vid_Vid_object_id` FOREIGN KEY (Vid_object_id) REFERENCES Vid_object (Vid_object_id));";
        public const string Type_Insurance = "CREATE TABLE Type_Insurance (Type_Insurance_id INTEGER AUTO_INCREMENT NOT NULL, title VARCHAR (50) NOT NULL,Vid_Insurance_id INTEGER NOT NULL, discription VARCHAR (50), PRIMARY KEY(Type_Insurance_id), CONSTRAINT `fk_Vid_Insurance` FOREIGN KEY (Vid_Insurance_id) REFERENCES Vid_Insurance (Vid_Insurance_id));";
        public const string Users = "CREATE TABLE Users (User_id INTEGER AUTO_INCREMENT NOT NULL, Login VARCHAR (100) NOT NULL, Password VARCHAR (100) NOT NULL, PRIMARY KEY(User_id));";
        public const string Vid_Insurance = "CREATE TABLE Vid_Insurance (Vid_Insurance_id INTEGER AUTO_INCREMENT NOT NULL, title VARCHAR (30) NOT NULL, PRIMARY KEY(Vid_Insurance_id));";
        public const string Vid_object = "CREATE TABLE Vid_object (Vid_object_id INTEGER AUTO_INCREMENT NOT NULL, title VARCHAR (100) NOT NULL, PRIMARY KEY(Vid_object_id));";
        public static string Parse(string DDL)
        {
            string result = DDL.Replace("\n", "");
            result = result.Replace("\r", "");

            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex("[ ]{2,}", options);
            result = regex.Replace(result, " ");

            return result;
        }

        public readonly static Dictionary<string, string> Tables = new Dictionary<string, string>
        {
            {"users", Users },
            {"client", Client },
            {"vid_object", Vid_object },
            {"vid_insurance", Vid_Insurance},
            {"type_insurance", Type_Insurance},
            {"object_vid", Object_vid },
            {"object_insurance", Object_Insurance },
            {"contract", Contract },
            {"document", Document },

        };
    }
}
