﻿using Abstraction.Entity;
using MariaDB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data
{
    class DatabaseObject_vid : Abstraction.Database.IDatabaseObject_vid
    {
        private DatabaseReadWrite<Object_vid> ReadWrite = new DatabaseReadWrite<Object_vid>() { EntityKey = Object_vid.PK, EntityName = "Object_Vid" };
        public IObject_vid CreateOne() => new Object_vid();

        public int Delete(int id)
        {
            return Convert.ToInt32(ReadWrite.Delete(id));
        }

        public List<IObject_vid> Get()
        {
            List<IObject_vid> resault = new List<IObject_vid>();
            resault.AddRange(ReadWrite.Get());
            return resault;
        }

        public IObject_vid Get(int id)
        {
            return ReadWrite.Get(id);
        }

        public int Post(IObject_vid value)
        {
            return Convert.ToInt32(ReadWrite.Post((Object_vid)value));
        }

        public int Put(int id, IObject_vid value)
        {
            return Convert.ToInt32(ReadWrite.Put(id,(Object_vid)value));
        }
    }
}
