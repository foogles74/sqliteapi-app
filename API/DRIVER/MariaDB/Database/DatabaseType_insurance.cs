﻿using Abstraction.Entity;
using MariaDB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data
{
    class DatabaseType_insurance : Abstraction.Database.IDatabaseType_insurance
    {
        private DatabaseReadWrite<Type_Insurance> ReadWrite = new DatabaseReadWrite<Type_Insurance>() { EntityKey = Type_Insurance.PK, EntityName = "Type_Insurance" };
        public IType_Insurance CreateOne() => new Type_Insurance();

        public int Delete(int id)
        {
            return Convert.ToInt32(ReadWrite.Delete(id));
        }

        public List<IType_Insurance> Get()
        {
            List<IType_Insurance> resault = new List<IType_Insurance>();
            resault.AddRange(ReadWrite.Get());
            return resault;
        }

        public IType_Insurance Get(int id)
        {
            return ReadWrite.Get(id);
        }

        public int Post(IType_Insurance value)
        {
            return Convert.ToInt32(ReadWrite.Post((Type_Insurance)value));
        }

        public int Put(int id, IType_Insurance value)
        {
            return Convert.ToInt32(ReadWrite.Put(id,(Type_Insurance)value));
        }
    }
}
