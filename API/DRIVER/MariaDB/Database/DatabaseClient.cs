﻿using Abstraction.Entity;
using Data;
using MariaDB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data
{
    public class DatabaseClient : Abstraction.Database.IDatabaseClient 
    {
        private DatabaseReadWrite<Client> ReadWrite = new DatabaseReadWrite<Client>() { EntityKey = Client.PK, EntityName = "Client" };
        
        public IClient CreateOne() 
        {
            return new Client(); 
        }

        public int Delete(int id)
        {
            return Convert.ToInt32(ReadWrite.Delete(id));
        }

        public List<IClient> Get()
        {
            List<IClient> resault = new List<IClient>();
            resault.AddRange(ReadWrite.Get());
            return resault;
        }


        public  IClient Get(int id) 
        {
            return ReadWrite.Get(id);
        }

        public int Post(IClient value)
        {
            object? resault = ReadWrite.Post((Client)value);
            return Convert.ToInt32(resault);
        }

        public int Put(int id, IClient value)
        {
            object? resault = ReadWrite.Put(id,(Client)value);
            return Convert.ToInt32(resault);
        }
        public DatabaseClient() {}
    }
}
