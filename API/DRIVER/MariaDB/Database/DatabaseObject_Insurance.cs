﻿using Abstraction.Entity;
using MariaDB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data
{
    class DatabaseObject_Insurance : Abstraction.Database.IDatabaseObject_Insurance
    {
        private DatabaseReadWrite<Object_Insurance> ReadWrite = new DatabaseReadWrite<Object_Insurance>() { EntityKey = Object_Insurance.PK, EntityName = "Object_Insurance" };
        public IObject_Insurance CreateOne() => new Object_Insurance();

        public int Delete(int id)
        {
            return Convert.ToInt32(ReadWrite.Delete(id));
        }

        public List<IObject_Insurance> Get()
        {
            List<IObject_Insurance> resault = new List<IObject_Insurance>();
            resault.AddRange(ReadWrite.Get());
            return resault;
        }

        public IObject_Insurance Get(int id)
        {
            return ReadWrite.Get(id);
        }

        public int Post(IObject_Insurance value)
        {
            return Convert.ToInt32(ReadWrite.Post((Object_Insurance)value));
        }

        public int Put(int id, IObject_Insurance value)
        {
            return Convert.ToInt32(ReadWrite.Put(id, (Object_Insurance)value));
        }
    }
}
