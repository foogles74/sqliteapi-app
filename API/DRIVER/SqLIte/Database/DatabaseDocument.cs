﻿using Abstraction.Database;
using Abstraction.Entity;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data
{
    class DatabaseDocument : Abstraction.Database.IDatabaseDocument
    {
        private DatabaseReadWrite<Document> ReadWrite = new DatabaseReadWrite<Document>() { EntityKey = Document.PK, EntityName = "Document" };
        public IDocument CreateOne() => new Document();

        public int Delete(int id)
        {
            return Convert.ToInt32(ReadWrite.Delete(id));
        }

        public List<IDocument> Get()
        {
            List<IDocument> resault = new List<IDocument>();
            resault.AddRange(ReadWrite.Get());
            return resault;
        }

        public IDocument Get(int id)
        {
            return ReadWrite.Get(id);
        }

        public int Post(IDocument value)
        {
            return Convert.ToInt32(ReadWrite.Post((Document)value));
        }

        public int Put(int id, IDocument value)
        {
            return Convert.ToInt32(ReadWrite.Put(id, (Document)value));
        }
    }
}
