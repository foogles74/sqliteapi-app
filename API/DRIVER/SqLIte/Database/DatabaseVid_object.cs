﻿using Abstraction.Entity;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data
{
    class DatabaseVid_object : Abstraction.Database.IDatabaseVid_object
    {
        private DatabaseReadWrite<Vid_object> ReadWrite = new DatabaseReadWrite<Vid_object>() { EntityKey = Vid_object.PK, EntityName = "Vid_Object" };
        public IVid_object CreateOne() => new Vid_object();

        public int Delete(int id)
        {
            return Convert.ToInt32(ReadWrite.Delete(id));
        }

        public List<IVid_object> Get()
        {
            List<IVid_object> resault = new List<IVid_object>();
            resault.AddRange(ReadWrite.Get());
            return resault;
        }

        public IVid_object Get(int id)
        {
            return ReadWrite.Get(id);
        }

        public int Post(IVid_object value)
        {
            return Convert.ToInt32(ReadWrite.Post((Vid_object)value));
        }

        public int Put(int id, IVid_object value)
        {
            return Convert.ToInt32(ReadWrite.Put(id, (Vid_object)value));
        }
    }
}
