﻿using Abstraction.Entity;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data
{
    class DatabaseVid_Insurance : Abstraction.Database.IDatabaseVid_Insurance
    {
        private DatabaseReadWrite<Vid_Insurance> ReadWrite = new DatabaseReadWrite<Vid_Insurance>() { EntityKey = Vid_Insurance.PK, EntityName = "Vid_insurance" };
        public IVid_Insurance CreateOne() => new Vid_Insurance();
        public int Delete(int id)
        {
            return Convert.ToInt32(ReadWrite.Delete(id));
        }

        public List<IVid_Insurance> Get()
        {
            List<IVid_Insurance> resault = new List<IVid_Insurance>();
            resault.AddRange(ReadWrite.Get());
            return resault;
        }

        public IVid_Insurance Get(int id)
        {
            return ReadWrite.Get(id);
        }

        public int Post(IVid_Insurance value)
        {
            return Convert.ToInt32(ReadWrite.Post((Vid_Insurance)value));
        }

        public int Put(int id, IVid_Insurance value)
        {
            return Convert.ToInt32(ReadWrite.Put(id, (Vid_Insurance)value));
        }
    }
}
