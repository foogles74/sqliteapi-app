﻿using Abstraction.Entity;
using SqLite.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data
{
    class DatabaseContract : Abstraction.Database.IDatabaseContract
    {
        private DatabaseReadWrite<Contract> ReadWrite = new DatabaseReadWrite<Contract>() { EntityKey = Contract.PK, EntityName = "Contract" };
        public IContract CreateOne() => new Contract();

        public int Delete(int id)
        {
            return Convert.ToInt32(ReadWrite.Delete(id));
        }

        public List<IContract> Get()
        {
            List<IContract> resault = new List<IContract>();
            resault.AddRange(ReadWrite.Get());
            return resault;
        }

        public IContract Get(int id)
        {
            return ReadWrite.Get(id);
        }

        public int Post(IContract value)
        {
            return Convert.ToInt32(ReadWrite.Post((Contract)value));
        }

        public int Put(int id, IContract value)
        {
            return Convert.ToInt32(ReadWrite.Put(id, (Contract)value));
        }
    }
}
