﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace SqLite.Data
{
    internal class DatabaseDDL
    {
        public const string Client = "CREATE TABLE Client (Clients_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Phone STRING NOT NULL, Pasport STRING NOT NULL, Polise STRING NOT NULL, FIO STRING NOT NULL)";
        public const string Contract = "CREATE TABLE Contract (Contract_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Client_id INTEGER REFERENCES Client (Clients_id) NOT NULL, Date_start DATE NOT NULL, Date_end DATE NOT NULL, sum_insurance DOUBLE NOT NULL, Type_Insurance_id INTEGER REFERENCES Type_Insurance (Type_Insurance_id) NOT NULL, Name STRING NOT NULL)";
        public const string Document = "CREATE TABLE Document (Document_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Contract_id INTEGER REFERENCES Contract (Contract_id) NOT NULL, Object_Insurance_id INTEGER REFERENCES Object_Insurance (Object_Insurance_id) NOT NULL)";
        public const string Object_Insurance = "CREATE TABLE Object_Insurance (Object_Insurance_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Discripton VARCHAR (100), Price DOUBLE NOT NULL, Vid_object_id INTEGER REFERENCES Vid_object (Vid_object_id) NOT NULL)";
        public const string Object_vid = "CREATE TABLE Object_vid (Object_vid_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Vid_insurance_id INTEGER REFERENCES Vid_Insurance (Vid_Insurance_id) NOT NULL, Vid_object_id INTEGER NOT NULL REFERENCES Vid_object (Vid_object_id))";
        public const string Type_Insurance = "CREATE TABLE Type_Insurance (Type_Insurance_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR (50) NOT NULL, Vid_Insurance_id INTEGER REFERENCES Vid_Insurance (Vid_Insurance_id) NOT NULL, discription VARCHAR (50))";
        public const string Users = "CREATE TABLE Users (User_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, Login STRING NOT NULL, Password STRING NOT NULL)";
        public const string Vid_Insurance = "CREATE TABLE Vid_Insurance (Vid_Insurance_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR (30) NOT NULL)";
        public const string Vid_object = "CREATE TABLE Vid_object (Vid_object_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR NOT NULL)";
        
        public static string Parse(string DDL)
        {
            string result = DDL.Replace("\n", "");
            result = result.Replace("\r", "");

            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex("[ ]{2,}", options);
            result = regex.Replace(result, " ");

            return result;
        }

        public readonly static Dictionary<string, string> Tables = new Dictionary<string, string>
        {
            {"Users", Users },
            {"Client", Client },
            {"Vid_object", Vid_object },
            { "Vid_Insurance", Vid_Insurance},
            {"Type_Insurance", Type_Insurance},
            {"Object_vid", Object_vid },
            {"Object_Insurance", Object_Insurance },
            {"Contract", Contract },
            {"Document", Document },
            
        };          

    }


}
