﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace SqLite.Data
{
    internal class DataUtils<T, X>
        where T : class, new()
        where X : DatabaseReadWrite<T>
    {
        private const string keysSepareator = ", ";
        public static SqliteCommand GetInstertCommand(T value, X dataClass) 
            
        {
            SqliteCommand command = new SqliteCommand();
            List<string> fields = new List<string>();
            List <SqliteParameter> parameters = FillParametrs(value, dataClass.EntityKey);
            string keys = "";
            string values = "";
            parameters.ForEach(p =>
            {
                keys += $"{p.ParameterName}{keysSepareator}";
                values += $"${p.ParameterName}{keysSepareator}";
            });
            keys = RemoveLastKeySeparator(keys);
            values = RemoveLastKeySeparator(values);
            command.CommandText = $"INSERT INTO {dataClass.EntityName} ({keys}) VALUES({values})";
            return command;
        }
        private static List<SqliteParameter> FillParametrs(T value, string key)
        {
            List<SqliteParameter> result = new List<SqliteParameter>();
            PropertyInfo[] properties = value.GetType().GetProperties();
            foreach (PropertyInfo property in properties)
            {
                if (property.Name.ToString() != key)
                {
                    object? item = property.GetValue(value);
                    if(item != null)
                    {
                        result.Add(new SqliteParameter(property.Name.ToString(), item));
                    }
                }
            }
            return result;
        }
        private static string RemoveLastKeySeparator(string value)
        {
            if (value.EndsWith(keysSepareator))
            {
                return value.Remove(value.Length - keysSepareator.Length, keysSepareator.Length);
            }
            else
            {
                return value;
            }
            
        }
    }
}
