﻿using Abstraction;
using Abstraction.Database;
using Abstraction.Entity;
using Microsoft.Data.Sqlite;
using System;
using SqLite.Data;
using System.Reflection;
using System.Collections.Generic;


namespace Data
{
    public class Database : IDatabase
    {
        private SqliteConnection connection;
        public IDatabaseClient Client => new DatabaseClient();

        public IDatabaseContract Contract => new DatabaseContract();

        public IDatabaseObject_Insurance Object_Insurance => new DatabaseObject_Insurance();

        public IDatabaseDocument Document => new DatabaseDocument();

        public IDatabaseObject_vid Object_Vid => new DatabaseObject_vid();

        public IDatabaseType_insurance Type_Insurance => new DatabaseType_insurance();

        public IDatabaseVid_Insurance Vid_Insurance => new DatabaseVid_Insurance();

        public IDatabaseVid_object Vid_Object => new DatabaseVid_object();

        public IDatabaseUsers Users => new DatabaseUsers();

        public Database(IDatabaseSetting settings)
        {
            if (DAO.Connect(settings))
            {
                if (CheckDatabase())
                {
                    Console.WriteLine("База подключена");
                }
                else 
                {
                    throw new Exception("Verifacted failed");
                }
            }
            else 
            {
                throw new Exception("Wrong settings!");
            }
        }
        private bool CheckOrCreateDatabase()
        {
            bool result = false;
            try
            {
                connection.Open();
                SqliteCommand command = connection.CreateCommand();
                command.CommandText = ".schema ";
                connection.Close();
                result = true;
            }
            catch
            {
                ;
            }
            return result;
        }
        private bool CheckDatabase() 
        {
            bool resault = false;
            foreach (KeyValuePair<string, string> item in DatabaseDDL.Tables)
            {
                resault = CheckDatabaseClient(item.Key);
            }
            return resault;
        }
        private bool CheckDatabaseClient(string tablename ) 
        {
            bool resault = false;
            Dictionary<string,string> DLL = DatabaseDDL.Tables;
            string dllname = DLL[key: tablename] ;
            DAO dao = DAO.Instance;
            object? exsist = dao.GetSingle($"SELECT sql FROM sqlite_master WHERE type = 'table' AND name = '{tablename}';");
            if (exsist == null)
            {
                int? created = dao.ExecuteNonQuery(dllname);
                if (created != null) 
                {
                    resault = true;
                }
            }
            else 
            {
               object? data = dao.GetSingle($"SELECT sql FROM sqlite_schema WHERE name = '{tablename}';");
                if (data != null)
                {
                    string ddl = DatabaseDDL.Parse(data.ToString());
                    resault = ddl == DatabaseDDL.Parse(dllname);
                }
            }
            return resault;
        }
    }

}