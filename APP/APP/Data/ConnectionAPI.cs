﻿using Abstraction.Entity;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Security.Policy;
using System.Text.Json;
using System.Threading.Tasks;

namespace APP.Data
{
    public class ConnectionAPI<T> where T : class,new()
    {
        private const string Url = "https://localhost:5001";
        private static ConnectionAPI<T> instance = new ConnectionAPI<T>();
        public static ConnectionAPI<T> Instance { get { return instance; } }
        private RestClient client;
        private const string ClientId = "bebra";
        private const string ClientSecret = "kek";
        private ConnectionAPI()
        {
            client = new RestClient(Url);
        }
        public static HttpStatusCode Login(string login, string password) 
        {
            string urlapi = Url + "/connect/token";
            RestRequest request = new RestRequest(urlapi) { Method= Method.Post};
            request.AddParameter("Accept", "application/json", ParameterType.HttpHeader);
            request.AddParameter("Content-Type", "application/x-www-form-urlencoded", ParameterType.HttpHeader);
            request.AddParameter("client_id", ClientId, ParameterType.GetOrPost);
            request.AddParameter("client_secret", ClientSecret, ParameterType.GetOrPost);
            request.AddParameter("grant_type", "password", ParameterType.GetOrPost);
            request.AddParameter("username", login, ParameterType.GetOrPost);
            request.AddParameter("password", password, ParameterType.GetOrPost);
            var responce =  Instance.client.Execute(request);
            if (responce.StatusCode == HttpStatusCode.OK)
            {
                if (GetPassword(login, password))
                {
                    AuthToken auth = JsonSerializer.Deserialize<AuthToken>(responce.Content);
                    Properties.Settings.Default.Token = auth.access_token;
                }
                else return HttpStatusCode.InternalServerError;
            }
            return responce.StatusCode;
        }
        private static bool GetPassword(string login, string password) 
        {
            bool resault = false;
            
            string urlapi = Url + $"/api/Users/{login}/{password}";
            RestRequest request = new RestRequest(urlapi) { Method = Method.Get };
            Users users =  Instance.client.Get<Users>(request);
            if (login == users.login & password == users.password) 
            { resault = true; }
            return resault;
        }

        public static async Task<List<T>> Get(string url)
        {
            string urlapi = Url + $"/api/{url}";
            RestRequest request = new RestRequest(urlapi) { Method = Method.Get };
            request.AddHeader("authorization", "Bearer " + Properties.Settings.Default.Token);
            var responce = await Instance.client.GetAsync<List<T>>(request);
            return responce;
        }
        public static async Task<T> Get(string url,int id)
        {
            string urlapi = Url + $"/api/{url}/{id}";
            RestRequest request = new RestRequest(urlapi) { Method = Method.Get };
            request.AddHeader("authorization", "Bearer " + Properties.Settings.Default.Token);
            var responce = await Instance.client.GetAsync<T>(request);
            return responce;
        }
        public static async Task<bool> Post(string json, string url)
        {
            string urlapi = Url + $"/api/{url}";
            RestRequest request = new RestRequest(urlapi) { Method = Method.Post };
            request.AddHeader("authorization", "Bearer " + Properties.Settings.Default.Token);
            request.AddJsonBody(json);
            var responce = await Instance.client.PostAsync(request);
            if (responce.IsSuccessful)
            {
                return true;
            }
            else { return false; }
        }
        public static async Task<bool> Put(string json, int id, string url)
        {
            string urlapi = Url + $"/api/{url}/{id}";
            RestRequest request = new RestRequest(urlapi) { Method = Method.Put };
            request.AddHeader("authorization", "Bearer " + Properties.Settings.Default.Token);
            request.AddJsonBody(json);
            var responce = await Instance.client.PutAsync(request);
            if (responce.IsSuccessful)
            {
                return true;
            }
            else { return false; }
        }
        public static async Task<string> Delete(int id, string url)
        {
            string urlapi = Url + $"/api/{url}/{id}";
            RestRequest request = new RestRequest(urlapi) { Method = Method.Delete };
            request.AddHeader("authorization", "Bearer " + Properties.Settings.Default.Token);
            return  Instance.client.Delete(request).Content;
        }

    }
}
