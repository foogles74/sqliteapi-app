﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using Word = Microsoft.Office.Interop.Word;

namespace APP.Data
{
    internal class WordHelper
    {
        private FileInfo fileInfo;
        public WordHelper(string filename)
        {
            if (File.Exists(filename))
            {
                fileInfo = new FileInfo(filename);   
            }
            else 
            {
                throw new Exception("Файл не существует");
            }
        }

        internal async  Task<string> Process(Dictionary<string, string> items)
        {
            Word.Application app = null;
            try
            {
                app = new Word.Application();
                Object file = fileInfo.FullName;
                Object missing = Type.Missing;
                app.Documents.Open(file);
                foreach (var item in items)
                {
                    Word.Find find = app.Selection.Find;
                    find.Text = item.Key;
                    find.Replacement.Text = item.Value;

                    Object wrap = Word.WdFindWrap.wdFindContinue;
                    Object replacement = Word.WdReplace.wdReplaceAll;

                    find.Execute(FindText: Type.Missing,
                        MatchCase: false,
                        MatchWholeWord: false,
                        MatchWildcards: false,
                        MatchSoundsLike: missing,
                        Format: false,
                        Wrap: wrap,
                        ReplaceWith: missing,
                        Replace: replacement);

                }
                Object newFileName = Path.Combine(fileInfo.DirectoryName, DateTime.Now.ToString("yyyyMMdd HHmmss ") + fileInfo.Name);
                app.ActiveDocument.SaveAs(newFileName);
                app.ActiveDocument.Close();
                if (app != null)
                {
                    app.Quit();
                    return newFileName.ToString();
                }
                else { return null; }
            }
            catch
            {
                if (app != null)
                    app.Quit();
                return null;
            }
            finally {
                if (app != null)
                {
                    app.Quit();
                }
            }

            
        }
    }
}
