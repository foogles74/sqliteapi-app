﻿using Abstraction.Entity;
using APP.Data;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using Abstraction.Database;
using System.Collections;

namespace APP.Forms
{
    public partial class FormObject_vid : Form
    {
        private bool res = true;
        private int idput = 0;
        private string TableName = "Object_Vid";
        private Dictionary<string, long> vilist = new Dictionary<string, long>();
        private Dictionary<string, long> volist = new Dictionary<string, long>();
        private Dictionary<long, string> vilist1 = new Dictionary<long, string>();
        private Dictionary<long, string> vilist2 = new Dictionary<long, string>();
        public FormObject_vid(bool meyhod, string id)
        {
            InitializeComponent();
            Vid_obj.DropDownStyle = ComboBoxStyle.DropDownList;
            Vid_ins.DropDownStyle = ComboBoxStyle.DropDownList;
            GetVid_insurance();
            GetVid_object();
            if (meyhod) { res = true; }
            else { res = false; idput = int.Parse(id); Put(); }
        }
        private async void Put()
        {
            Object_vid client = await ConnectionAPI<Object_vid>.Get(TableName, idput);
            Vid_ins.Text = vilist1[client.vid_insurance_id].ToString();
            Vid_obj.Text = vilist2[client.vid_object_id].ToString();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            bool resaul = false;
            if (Vid_ins.Text != "" & Vid_obj.Text != "")
            {
                JObject jArray = new JObject();
                jArray.Add("vid_insurance_id",int.Parse(vilist[Vid_ins.Text].ToString()));
                jArray.Add("vid_object_id", int.Parse(volist[Vid_obj.Text].ToString()));
                string json = JsonConvert.SerializeObject(jArray);
                if (res)
                {
                    resaul = await ConnectionAPI<Object_vid>.Post(json, TableName);
                }
                else
                {
                    resaul = await ConnectionAPI<Object_vid>.Put(json, idput, TableName);
                }
                if (resaul)
                {
                    Close();
                }
                else { MessageBox.Show("Не удалось вставить запись"); }
            }
            else { MessageBox.Show("Не все данные заполнены"); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
        private async void GetVid_insurance()
        {
            List<Vid_Insurance> type_Insurances = await ConnectionAPI<Vid_Insurance>.Get("Vid_insurance");
            foreach (Vid_Insurance i in type_Insurances)
            {
                Vid_ins.Items.Add(i.title);
                vilist.Add(i.title, i.Vid_Insurance_id);
                vilist1.Add(i.Vid_Insurance_id, i.title);
            }
        }
        private async void GetVid_object()
        {
            List<Vid_object> type_Insurances = await ConnectionAPI<Vid_object>.Get("Vid_Object");
            foreach (Vid_object i in type_Insurances)
            {
                Vid_obj.Items.Add(i.title);
                volist.Add(i.title, i.Vid_object_id);
                vilist2.Add(i.Vid_object_id, i.title);
            }
        }

    }
}
