﻿using Abstraction.Entity;
using APP.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace APP.Forms
{
    public partial class FormClient : Form
    {
        private bool res = true;
        private int idput = 0;
        private string TableName = "Client";
        public FormClient(bool meyhod, string id)
        {
            InitializeComponent();
            
            if (meyhod) { res = true; }
            else { res = false; idput = int.Parse(id); Put(); }
        }
        private async void Put()
        {
            Client client = await ConnectionAPI<Client>.Get(TableName, idput);
            Phone.Text = client.phone.ToString();
            Polise.Text = client.polise.ToString();
            Pasport.Text = client.pasport.ToString();
            FIO.Text = client.FIO.ToString();
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private async void ADD_Click(object sender, EventArgs e)
        {
            bool resaul = false;
            Regex regex = new Regex("[0-9]");
            if (regex.Matches(Phone.Text).Count == 11 &  regex.Matches(Polise.Text).Count == 16 &  regex.Matches(Pasport.Text).Count == 10  )
            {
                JObject jArray = new JObject();
                jArray.Add("phone", Phone.Text.ToString());
                jArray.Add("polise", Polise.Text.ToString());
                jArray.Add("pasport", Pasport.Text.ToString());
                jArray.Add("FIO", FIO.Text.ToString());
                string json = JsonConvert.SerializeObject(jArray);
                if (res)
                {
                    resaul = await ConnectionAPI<Client>.Post(json, TableName);
                }
                else
                {
                    resaul = await ConnectionAPI<Client>.Put(json, idput, TableName);
                }
                if (resaul)
                {
                    Close();
                }
                else { MessageBox.Show("Не удалось вставить запись"); }
            }
            else { MessageBox.Show("Не все данные заполнены"); }

        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormClient_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void FIO_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !(char.IsLetter(e.KeyChar) || e.KeyChar == (char)Keys.Back ||  e.KeyChar == (char)Keys.Space);
        }

        private void FormClient_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SelectNextControl(ActiveControl, true, true, true, true);
                e.Handled = true;
                ADD_Click(sender, e);
            }
        }
        private void Pasport_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void Polise_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void Phone_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void Pasport_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }
    }
}
