﻿using Abstraction.Entity;
using APP.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace APP.Forms
{
    public partial class FormPrice : Form
    {
        public FormPrice()
        {
            InitializeComponent();
            Get();
        }
        private async void Get()
        {
            List<Vid_object> Vid = await ConnectionAPI<Vid_object>.Get("Vid_Object");
            List<Object_Insurance> Object = await ConnectionAPI<Object_Insurance>.Get("Object_Insurance");
            dataGridView1.ColumnCount = 3;
            dataGridView1.Columns[0].Name = "Объект";
            dataGridView1.Columns[1].Name = "Вид объекта";
            dataGridView1.Columns[2].Name = "Цена";
            
            foreach (Object_Insurance a in Object)
            {
                string x = null;
                foreach (Vid_object b in Vid) 
                {
                    if (a.vid_Object_id == b.Vid_object_id)
                    {
                        x = b.title;
                    }
                }
                if (x != null)
                { 
                    
                    dataGridView1.Rows.Add(a.discripton.ToString(),x, a.Price.ToString());
                }  
            }
        }
    }
}
