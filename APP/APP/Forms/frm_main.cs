﻿using Abstraction.Entity;
using APP.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using Contract = Abstraction.Entity.Contract;

namespace APP.Forms
{
    public partial class frm_main : Form
    {
        private string Url;
        private string tables;
        public frm_main()
        {
            InitializeComponent();
            CreateDocumentBTN.Visible = false;
            dataGridView1.Visible = false;
            ADD.Visible = false;
            Change.Visible = false;
            Delete.Visible = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.ReadOnly = true;
            ADD.Enabled = false;
            Change.Enabled = false;
            Delete.Enabled = false;
        }
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) { }
        private void Change_Click(object sender, EventArgs e)
        {
            PostTable(false);
        }

        private async void ADD_Click(object sender, EventArgs e)
        {
            PostTable(true);
        }

        private async void Delete_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Удалить", "Уверены?", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    string row = dataGridView1[0, dataGridView1.CurrentRow.Index].Value.ToString();
                    var responce =await ConnectionAPI<Users>.Delete(Convert.ToInt32(row), Url);
                    if (responce != null & responce != "0")
                    {
                        UpdateTable();
                    }
                    else { MessageBox.Show("Не возможно удалить запись, эта запись где то ипользуется!"); }
                }
                catch { MessageBox.Show("Не возможно удалить запись!"); }
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTable();
        }
        private async void UpdateTable()
        {
            CreateDocumentBTN.Visible = false;
            dataGridView1.Visible = true;
            ADD.Visible = true;
            Change.Visible = true;
            Delete.Visible = true;
            try
            {
                string Table = null;
                if (tables == null)
                {
                    
                }
                else 
                {
                    Table = tables;
                }
                
                List<Client> clients = await ConnectionAPI<Client>.Get("Client");
                List<Contract> contarct = await ConnectionAPI<Contract>.Get("Contract");
                List<Document> documevt = await ConnectionAPI<Document>.Get("Document");
                List<Object_Insurance> object_Insurances = await ConnectionAPI<Object_Insurance>.Get("Object_Insurance");
                List<Object_vid> object_Vids = await ConnectionAPI<Object_vid>.Get("Object_vid");
                List<Type_Insurance> type_ins = await ConnectionAPI<Type_Insurance>.Get("Type_Insurance");
                List<Vid_Insurance> vid_Insurances = await ConnectionAPI<Vid_Insurance>.Get("Vid_Insurance");
                List<Vid_object> vid_Objects = await ConnectionAPI<Vid_object>.Get("Vid_object");
                switch (Table)
                {
                    case "Клиенты":
                        RefreshGridview();
                        dataGridView1.ColumnCount = 5;
                        dataGridView1.Columns[0].Name = "id";
                        dataGridView1.Columns[1].Name = "ФИО";
                        dataGridView1.Columns[2].Name = "Телефон";
                        dataGridView1.Columns[3].Name = "Паспорт";
                        dataGridView1.Columns[4].Name = "Полис";
                        this.dataGridView1.Columns["id"].Visible = false;
                        foreach (Client _Object in clients)
                        {
                            dataGridView1.Rows.Add(_Object.clients_id, _Object.FIO, _Object.phone, _Object.pasport, _Object.polise);
                        }
                        dataGridView1.Refresh();
                        Url = "Client";
                        break;
                    case "Контракт":
                        RefreshGridview();
                        dataGridView1.ColumnCount = 7;
                        dataGridView1.Columns[0].Name = "id";
                        dataGridView1.Columns[1].Name = "ФИО";
                        dataGridView1.Columns[2].Name = "Дата начала";
                        dataGridView1.Columns[3].Name = "Дата окончания";
                        dataGridView1.Columns[4].Name = "Сумма на страховку";
                        dataGridView1.Columns[5].Name = "Тип страхования";
                        dataGridView1.Columns[6].Name = "Название документа";
                        Dictionary<long, string> data4 = new Dictionary<long, string>();
                        foreach (Client i in clients)
                        {
                            try { data4.Add(i.clients_id, i.FIO); } catch { }
                        }
                        Dictionary<long, string> data5 = new Dictionary<long, string>();
                        foreach (Type_Insurance i in type_ins)
                        {
                            try { data5.Add(i.Type_Insurance_id, i.title); } catch { }
                        }
                        foreach (Contract _Object in contarct)
                        {
                            dataGridView1.Rows.Add(_Object.contract_id, data4[_Object.client_id], _Object.date_start, _Object.date_end, _Object.sum_insurance, data5[_Object.type_Insurance_id],_Object.Name);
                        }
                        this.dataGridView1.Columns["id"].Visible = false;
                        dataGridView1.Refresh();
                        Url = "Contract";
                        break;
                    case "Вид Объекта":
                        RefreshGridview();
                        dataGridView1.ColumnCount = 2;
                        dataGridView1.Columns[0].Name = "id";
                        dataGridView1.Columns[1].Name = "Название";
                        foreach (Vid_object _Object in vid_Objects)
                        {
                            dataGridView1.Rows.Add(_Object.Vid_object_id, _Object.title);
                        }
                        this.dataGridView1.Columns["id"].Visible = false;
                        dataGridView1.Refresh();
                        Url = "Vid_object";
                        break;
                    case "Документ":
                        RefreshGridview();
                        dataGridView1.ColumnCount = 3;
                        dataGridView1.Columns[0].Name = "id";
                        dataGridView1.Columns[1].Name = "Контракт";
                        dataGridView1.Columns[2].Name = "Объект";
                        Dictionary<long, string> data6 = new Dictionary<long, string>();
                        foreach (Contract i in contarct)
                        {
                            try { data6.Add(i.contract_id, i.Name); } catch { }
                        }
                        Dictionary<long, string> data7 = new Dictionary<long, string>();
                        foreach (Object_Insurance i in object_Insurances)
                        {
                            try { data7.Add(i.object_Insurance_id, i.discripton); } catch { }
                        }
                        foreach (Document _Object in documevt)
                        {
                            dataGridView1.Rows.Add(_Object.document_id, data6[_Object.contract_id], data7[_Object.object_Insurance_id]);
                        }
                        this.dataGridView1.Columns["id"].Visible = false;
                        CreateDocumentBTN.Visible = true;
                        dataGridView1.Refresh();
                        Url = "Document";
                        break;
                    case "Объекты Страхования":
                        RefreshGridview();
                        Dictionary<long, string> data3 = new Dictionary<long, string>();
                        dataGridView1.ColumnCount = 4;
                        dataGridView1.Columns[0].Name = "id";
                        dataGridView1.Columns[1].Name = "Описание";
                        dataGridView1.Columns[2].Name = "Цена";
                        dataGridView1.Columns[3].Name = "Вид объекта";
                        foreach (Vid_object i in vid_Objects)
                        {
                            try { data3.Add(i.Vid_object_id, i.title); } catch { }
                        }
                        foreach (Object_Insurance _Object in object_Insurances)
                        {
                            dataGridView1.Rows.Add(_Object.object_Insurance_id, _Object.discripton, _Object.Price, data3[_Object.vid_Object_id]);
                        }
                        this.dataGridView1.Columns["id"].Visible = false;
                        dataGridView1.Refresh();
                        Url = "Object_Insurance";
                        break;
                    case "Object_vid":
                        RefreshGridview();
                        Dictionary<long, string> data2 = new Dictionary<long, string>();
                        Dictionary<long, string> data1 = new Dictionary<long, string>();
                        dataGridView1.ColumnCount = 3;
                        dataGridView1.Columns[0].Name = "id";
                        dataGridView1.Columns[1].Name = "Вид Объекта";
                        dataGridView1.Columns[2].Name = "Вид Страхования";
                        foreach (Vid_Insurance i in vid_Insurances)
                        {
                            try { data1.Add(i.Vid_Insurance_id, i.title); } catch { }
                        }
                        foreach (Vid_object i in vid_Objects)
                        {
                            try { data2.Add(i.Vid_object_id, i.title); } catch { }
                        }
                        foreach (Object_vid _Object in object_Vids)
                        {
                            dataGridView1.Rows.Add(_Object.object_vid_id, data2[_Object.vid_object_id], data1[_Object.vid_insurance_id]);
                        }
                        this.dataGridView1.Columns["id"].Visible = false;
                        dataGridView1.Refresh();
                        Url = "Object_vid";
                        break;
                    case "Тип Страховки":
                        RefreshGridview();
                        Dictionary<long, string> data = new Dictionary<long, string>();
                        dataGridView1.ColumnCount = 4;
                        dataGridView1.Columns[0].Name = "id";
                        dataGridView1.Columns[1].Name = "Название";
                        dataGridView1.Columns[2].Name = "Вид Страхования";
                        dataGridView1.Columns[3].Name = "Описание";
                        foreach (Vid_Insurance i in vid_Insurances)
                        {
                            data.Add(i.Vid_Insurance_id, i.title);
                        }
                        foreach (Type_Insurance _Object in type_ins)
                        {
                            dataGridView1.Rows.Add(_Object.Type_Insurance_id, _Object.title, data[_Object.Vid_Insurance_id], _Object.discription);
                        }
                        this.dataGridView1.Columns["id"].Visible = false;
                        dataGridView1.Refresh();
                        Url = "Type_Insurance";
                        break;
                    case "Вид Страховки":
                        RefreshGridview();
                        dataGridView1.ColumnCount = 2;
                        dataGridView1.Columns[0].Name = "id";
                        dataGridView1.Columns[1].Name = "Название";
                        foreach (Vid_Insurance _Object in vid_Insurances)
                        {
                            dataGridView1.Rows.Add(_Object.Vid_Insurance_id, _Object.title);
                        }
                        this.dataGridView1.Columns["id"].Visible = false;
                        dataGridView1.Refresh();
                        Url = "Vid_Insurance";
                        break;
                }
                ADD.Enabled = true;
                Change.Enabled = true;
                Delete.Enabled = true;
            }
            catch { MessageBox.Show("Таблица не выбрана!"); }
        }
        private void RefreshGridview()
        {
            dataGridView1.DataSource = null;
            dataGridView1.Rows.Clear();
            dataGridView1.Columns.Clear();
        }
        private void PostTable(bool method)
        {
            CreateDocumentBTN.Visible = false;
            string row = null;
            try { row = dataGridView1["id", dataGridView1.CurrentRow.Index].Value.ToString(); } catch { }
            try
            {
                string Table = null;
                if (tables == null) { }                
                else
                {
                    Table = tables;
                }


                switch (Table)
                {
                    case "Клиенты":
                        FormClient formClient = new FormClient(method, row);
                        formClient.ShowDialog();
                        UpdateTable();
                        break;
                    case "Контракт":
                        FormContract dc = new FormContract(method, row);
                        dc.ShowDialog();
                        UpdateTable();
                        CreateDocumentBTN.Visible = true;
                        break;
                    case "Вид Объекта":
                        FormVid_object fvo = new FormVid_object(method, row);
                        fvo.ShowDialog();
                        UpdateTable();
                        break;
                    case "Документ":
                        FormDocument fd = new FormDocument(method, row);
                        fd.ShowDialog();
                        UpdateTable();
                        break;
                    case "Объекты Страхования":
                        FormObject_Insurance formObject_Insurance = new FormObject_Insurance(method, row);
                        formObject_Insurance.ShowDialog();
                        UpdateTable();
                        break;
                    case "Object_vid":
                        FormObject_vid formObject_Vid = new FormObject_vid(method, row);
                        formObject_Vid.ShowDialog();
                        UpdateTable();
                        break;
                    case "Тип Страховки":
                        FormType_Insurance formType_Insurance = new FormType_Insurance(method, row);
                        formType_Insurance.ShowDialog();
                        UpdateTable();
                        break;
                    case "Вид Страховки":
                        FormVid_Insurance formVid_Object = new FormVid_Insurance(method, row);
                        formVid_Object.ShowDialog();
                        UpdateTable();
                        break;
                }
            }
            catch { }
        }

        private void Mouse_Enter_ADD(object sender, EventArgs e)
        {
            ADD.ForeColor = Color.Green;
        }
        private void Mouse_Enter_Change(object sender, EventArgs e)
        {
            Change.ForeColor = Color.Green;
        }
        private void Mouse_Enter_Delete(object sender, EventArgs e)
        {
            Delete.ForeColor = Color.Red;
        }
        private void Mouse_Leave_ADD(object sender, EventArgs e)
        {
            ADD.ForeColor = Color.Black;
        }
        private void Mouse_Leave_Change(object sender, EventArgs e)
        {
            Change.ForeColor = Color.Black;
        }
        private void Mouse_Leave_Delete(object sender, EventArgs e)
        {
            Delete.ForeColor = Color.Black;
        }


        private async void Reports(string reportname)
        {
            string report = reportname;
            switch (report)
            {
                case "По клинтем":
                    FormClientsDociment formClientsDociment = new FormClientsDociment();
                    formClientsDociment.Show();
                    break;
                case "Прайс лист":
                    FormPrice formprice = new FormPrice();
                    formprice.Show();
                    break;

            }
        }

        private void frm_main_Load(object sender, EventArgs e)
        {

        }

        private void вToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tables =  "Клиенты";
            UpdateTable();
        }

        private void видСтраховкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tables = "Вид Страховки";
            UpdateTable();
        }

        private void типСтраховкиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tables = "Тип Страховки";
            UpdateTable();
        }

        private void вToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            tables = "Вид Объекта";
            UpdateTable();
        }

        private void объектыСтрахованияToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            tables = "Объекты Страхования";
            UpdateTable();
        }

        private void документToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tables = "Документ";
            UpdateTable();
        }

        private void контрактToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tables = "Контракт";
            UpdateTable();
        }

        private void поКлинтемToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reports("По клинтем");
        }

        private void прайсЛистToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reports("Прайс лист");
        }

        private async void CreateDocumentBTN_Click(object sender, EventArgs e)
        {
            var helper = new WordHelper("pattern.doc");   
            try
            {  
                string row = dataGridView1[0, dataGridView1.CurrentRow.Index].Value.ToString();
                Document doc = await ConnectionAPI<Document>.Get("Document",Int32.Parse(row));
                Contract contr = await ConnectionAPI<Contract>.Get("Contract",Int32.Parse(doc.contract_id.ToString()));
                Client cli = await ConnectionAPI<Client>.Get("Client", Int32.Parse(contr.client_id.ToString()));
                var items = new Dictionary<string, string>
                {
                    {"<NumDoGov>", doc.contract_id.ToString()},
                    {"<Straxsum>",contr.sum_insurance.ToString()},
                    {"<datanach>", contr.date_start.ToString("D")},
                    {"<dataokon>",contr.date_end.ToString("D") },
                    {"<NameCLi>", cli.FIO.ToString()},
                    {"<God>", DateTime.Now.ToString("yyyy")}
                };
                string otevet =  await helper.Process(items);
                
                if (otevet != null) 
                {
                    MessageBox.Show("Форма договора успешно создана!");
                    try { Process.Start(new ProcessStartInfo(@otevet) { UseShellExecute = true }); } catch { MessageBox.Show("Файл создан в корневой папке приложения"); }
                    
                }
            }
            catch { MessageBox.Show("Должен быть устоновлен office 2013"); }

            
        }
    }
}
