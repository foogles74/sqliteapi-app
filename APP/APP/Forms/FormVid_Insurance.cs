﻿using Abstraction.Entity;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using APP.Data;

namespace APP.Forms
{
    public partial class FormVid_Insurance : Form
    {
        private bool res = true;
        private int idput = 0;
        private string TableName = "Vid_insurance";
        public FormVid_Insurance(bool meyhod, string id)
        {
            InitializeComponent();
            if (meyhod) { res = true; }
            else { res = false; idput = int.Parse(id); Put(); }
        }
        private async void Put()
        {
            Vid_Insurance client = await ConnectionAPI<Vid_Insurance>.Get(TableName, idput);
            title.Text = client.title.ToString();
        }
        private async void ADD_Click(object sender, EventArgs e)
        {
            bool resaul = false;
            if (title.Text != "")
            {
                JObject jArray = new JObject();
                jArray.Add("title", title.Text);
                string json = JsonConvert.SerializeObject(jArray);
                if (res)
                {
                    resaul = await ConnectionAPI<Vid_Insurance>.Post(json, TableName);
                }
                else
                {
                    resaul = await ConnectionAPI<Vid_Insurance>.Put(json, idput, TableName);
                }
                if (resaul)
                {
                    Close();
                }
                else { MessageBox.Show("Не удалось вставить запись"); }
            }
            else { MessageBox.Show("Не все данные заполнены"); }

        }
        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
