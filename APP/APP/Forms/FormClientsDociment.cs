﻿using Abstraction.Entity;
using APP.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace APP.Forms
{
    public partial class FormClientsDociment : Form
    {
        private string TableName = "Client";
        public FormClientsDociment()
        {
            InitializeComponent();
            Get();
        }
        private async void Get() 
        {
            List<Client> clients = await ConnectionAPI<Client>.Get("Client");
            dataGridView1.ColumnCount = 5;
            dataGridView1.Columns[0].Name = "id";
            dataGridView1.Columns[1].Name = "ФИО";
            dataGridView1.Columns[2].Name = "Телефон";
            dataGridView1.Columns[3].Name = "Паспорт";
            dataGridView1.Columns[4].Name = "Полис";
            this.dataGridView1.Columns["id"].Visible = false;
            foreach (Client _Object in clients)
            {
                dataGridView1.Rows.Add(_Object.clients_id, _Object.FIO, _Object.phone, _Object.pasport, _Object.polise);
            }
            dataGridView1.Refresh();
        }
    }
}
