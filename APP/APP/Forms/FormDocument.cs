﻿using Abstraction.Entity;
using APP.Data;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace APP.Forms
{
    public partial class FormDocument : Form
    {
        private bool res = true;
        private int idput = 0;
        private string TableName = "Document";

        private Dictionary<string, long> oilist = new Dictionary<string, long>();
        private Dictionary<string, long> oilist2 = new Dictionary<string, long>();
        private Dictionary<long, string> vilist1 = new Dictionary<long, string>();
        private Dictionary<long, string> vilist2 = new Dictionary<long, string>();
        public FormDocument(bool meyhod, string id)
        {
            InitializeComponent();
            GetClient();
            Gettypeinsur();
            Contra_id.DropDownStyle = ComboBoxStyle.DropDownList;
            Obj_ins.DropDownStyle = ComboBoxStyle.DropDownList;
            if (meyhod) { res = true; }
            else { res = false; idput = int.Parse(id); Put(); }
        }
        private async void Put()
        {
            Document client = await ConnectionAPI<Document>.Get(TableName, idput);
            Contra_id.Text = vilist1[client.contract_id].ToString();
            Obj_ins.Text = vilist2[client.object_Insurance_id].ToString();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            bool resaul = false;
            if (Contra_id.Text != "" & Obj_ins.Text != "")
            {
                JObject jArray = new JObject();
                jArray.Add("contract_id", int.Parse(oilist2[Contra_id.Text].ToString()));
                jArray.Add("object_Insurance_id", int.Parse(oilist[Obj_ins.Text].ToString()));
                string json = JsonConvert.SerializeObject(jArray);
                if (res)
                {
                    resaul = await ConnectionAPI<Document>.Post(json, TableName);
                }
                else
                {
                    resaul = await ConnectionAPI<Document>.Put(json, idput, TableName);
                }
                if (resaul)
                {
                    Close();
                }
                else { MessageBox.Show("Не удалось вставить запись"); }
            }
            else { MessageBox.Show("Не все данные заполнены"); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
        private async void GetClient()
        {
            List<Contract> type_Insurances = await ConnectionAPI<Contract>.Get("Contract");
            foreach (Contract i in type_Insurances)
            {
                Contra_id.Items.Add(i.Name);
                oilist2.Add(i.Name, i.contract_id);
                vilist1.Add(i.contract_id, i.Name);
            }
        }
        private async void Gettypeinsur()
        {
            List<Object_Insurance> type_Insurances = await ConnectionAPI<Object_Insurance>.Get("Object_Insurance");
            foreach (Object_Insurance i in type_Insurances)
            {
                Obj_ins.Items.Add(i.discripton);
                oilist.Add(i.discripton, i.object_Insurance_id);
                vilist2.Add(i.object_Insurance_id, i.discripton);
            }
        }
    }
}
