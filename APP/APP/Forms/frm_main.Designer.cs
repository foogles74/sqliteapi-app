﻿namespace APP.Forms
{
    partial class frm_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.clientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Delete = new System.Windows.Forms.Button();
            this.ADD = new System.Windows.Forms.Button();
            this.Change = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.вToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.страховниеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.видСтраховкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.типСтраховкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.объектыСтрахованияToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.документыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.контрактToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.документToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.отчетыToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.поКлинтемToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.прайсЛистToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateDocumentBTN = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientBindingSource)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(0, 24);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.Size = new System.Drawing.Size(1123, 548);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1.DoubleClick += new System.EventHandler(this.Change_Click);
            // 
            // clientBindingSource
            // 
            this.clientBindingSource.DataSource = typeof(Abstraction.Entity.Client);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(281, 3);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(142, 33);
            this.Delete.TabIndex = 1;
            this.Delete.Text = "Удалить";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            this.Delete.MouseEnter += new System.EventHandler(this.Mouse_Enter_Delete);
            this.Delete.MouseLeave += new System.EventHandler(this.Mouse_Leave_Delete);
            // 
            // ADD
            // 
            this.ADD.Location = new System.Drawing.Point(3, 3);
            this.ADD.Name = "ADD";
            this.ADD.Size = new System.Drawing.Size(142, 34);
            this.ADD.TabIndex = 2;
            this.ADD.Text = "Добавить";
            this.ADD.UseVisualStyleBackColor = true;
            this.ADD.Click += new System.EventHandler(this.ADD_Click);
            this.ADD.MouseEnter += new System.EventHandler(this.Mouse_Enter_ADD);
            this.ADD.MouseLeave += new System.EventHandler(this.Mouse_Leave_ADD);
            // 
            // Change
            // 
            this.Change.Location = new System.Drawing.Point(151, 3);
            this.Change.Name = "Change";
            this.Change.Size = new System.Drawing.Size(124, 33);
            this.Change.TabIndex = 3;
            this.Change.Text = "Изменить";
            this.Change.UseVisualStyleBackColor = true;
            this.Change.Click += new System.EventHandler(this.Change_Click);
            this.Change.MouseEnter += new System.EventHandler(this.Mouse_Enter_Change);
            this.Change.MouseLeave += new System.EventHandler(this.Mouse_Leave_Change);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.отчетыToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1123, 24);
            this.menuStrip1.TabIndex = 6;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.вToolStripMenuItem,
            this.страховниеToolStripMenuItem,
            this.документыToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(62, 20);
            this.toolStripMenuItem1.Text = "Данные";
            // 
            // вToolStripMenuItem
            // 
            this.вToolStripMenuItem.Name = "вToolStripMenuItem";
            this.вToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.вToolStripMenuItem.Text = "Клиенты";
            this.вToolStripMenuItem.Click += new System.EventHandler(this.вToolStripMenuItem_Click);
            // 
            // страховниеToolStripMenuItem
            // 
            this.страховниеToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.видСтраховкиToolStripMenuItem,
            this.типСтраховкиToolStripMenuItem,
            this.вToolStripMenuItem1,
            this.объектыСтрахованияToolStripMenuItem});
            this.страховниеToolStripMenuItem.Name = "страховниеToolStripMenuItem";
            this.страховниеToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.страховниеToolStripMenuItem.Text = "Страховние";
            // 
            // видСтраховкиToolStripMenuItem
            // 
            this.видСтраховкиToolStripMenuItem.Name = "видСтраховкиToolStripMenuItem";
            this.видСтраховкиToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.видСтраховкиToolStripMenuItem.Text = "Вид Страховки";
            this.видСтраховкиToolStripMenuItem.Click += new System.EventHandler(this.видСтраховкиToolStripMenuItem_Click);
            // 
            // типСтраховкиToolStripMenuItem
            // 
            this.типСтраховкиToolStripMenuItem.Name = "типСтраховкиToolStripMenuItem";
            this.типСтраховкиToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.типСтраховкиToolStripMenuItem.Text = "Тип Страховки";
            this.типСтраховкиToolStripMenuItem.Click += new System.EventHandler(this.типСтраховкиToolStripMenuItem_Click);
            // 
            // вToolStripMenuItem1
            // 
            this.вToolStripMenuItem1.Name = "вToolStripMenuItem1";
            this.вToolStripMenuItem1.Size = new System.Drawing.Size(197, 22);
            this.вToolStripMenuItem1.Text = "Вид Объекта";
            this.вToolStripMenuItem1.Click += new System.EventHandler(this.вToolStripMenuItem1_Click);
            // 
            // объектыСтрахованияToolStripMenuItem
            // 
            this.объектыСтрахованияToolStripMenuItem.Name = "объектыСтрахованияToolStripMenuItem";
            this.объектыСтрахованияToolStripMenuItem.Size = new System.Drawing.Size(197, 22);
            this.объектыСтрахованияToolStripMenuItem.Text = "Объекты Страхования";
            this.объектыСтрахованияToolStripMenuItem.Click += new System.EventHandler(this.объектыСтрахованияToolStripMenuItem_Click);
            // 
            // документыToolStripMenuItem
            // 
            this.документыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.контрактToolStripMenuItem,
            this.документToolStripMenuItem});
            this.документыToolStripMenuItem.Name = "документыToolStripMenuItem";
            this.документыToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.документыToolStripMenuItem.Text = "Документы";
            // 
            // контрактToolStripMenuItem
            // 
            this.контрактToolStripMenuItem.Name = "контрактToolStripMenuItem";
            this.контрактToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.контрактToolStripMenuItem.Text = "Контракт";
            this.контрактToolStripMenuItem.Click += new System.EventHandler(this.контрактToolStripMenuItem_Click);
            // 
            // документToolStripMenuItem
            // 
            this.документToolStripMenuItem.Name = "документToolStripMenuItem";
            this.документToolStripMenuItem.Size = new System.Drawing.Size(128, 22);
            this.документToolStripMenuItem.Text = "Документ";
            this.документToolStripMenuItem.Click += new System.EventHandler(this.документToolStripMenuItem_Click);
            // 
            // отчетыToolStripMenuItem
            // 
            this.отчетыToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.поКлинтемToolStripMenuItem,
            this.прайсЛистToolStripMenuItem});
            this.отчетыToolStripMenuItem.Name = "отчетыToolStripMenuItem";
            this.отчетыToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.отчетыToolStripMenuItem.Text = "Отчеты";
            // 
            // поКлинтемToolStripMenuItem
            // 
            this.поКлинтемToolStripMenuItem.Name = "поКлинтемToolStripMenuItem";
            this.поКлинтемToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.поКлинтемToolStripMenuItem.Text = "По клинтем";
            this.поКлинтемToolStripMenuItem.Click += new System.EventHandler(this.поКлинтемToolStripMenuItem_Click);
            // 
            // прайсЛистToolStripMenuItem
            // 
            this.прайсЛистToolStripMenuItem.Name = "прайсЛистToolStripMenuItem";
            this.прайсЛистToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.прайсЛистToolStripMenuItem.Text = "Прайс лист";
            this.прайсЛистToolStripMenuItem.Click += new System.EventHandler(this.прайсЛистToolStripMenuItem_Click);
            // 
            // CreateDocumentBTN
            // 
            this.CreateDocumentBTN.Location = new System.Drawing.Point(429, 3);
            this.CreateDocumentBTN.Name = "CreateDocumentBTN";
            this.CreateDocumentBTN.Size = new System.Drawing.Size(173, 33);
            this.CreateDocumentBTN.TabIndex = 7;
            this.CreateDocumentBTN.Text = "Создать Бланк страхования";
            this.CreateDocumentBTN.UseVisualStyleBackColor = true;
            this.CreateDocumentBTN.Click += new System.EventHandler(this.CreateDocumentBTN_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.Delete, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.CreateDocumentBTN, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.Change, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.ADD, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 533);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1123, 39);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1123, 572);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frm_main";
            this.Text = "Выбор Таблиц";
            this.Load += new System.EventHandler(this.frm_main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clientBindingSource)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource clientBindingSource;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button ADD;
        private System.Windows.Forms.Button Change;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem вToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem страховниеToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem видСтраховкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem типСтраховкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem объектыСтрахованияToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem документыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem контрактToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem документToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem отчетыToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem поКлинтемToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem прайсЛистToolStripMenuItem;
        private System.Windows.Forms.Button CreateDocumentBTN;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
    }
}