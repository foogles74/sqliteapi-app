﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Abstraction.Entity;
using APP.Data;

namespace APP.Forms
{
    public partial class FormVid_object : Form
    {
        private bool res = true;
        private int idput = 0;
        private string TableName = "Vid_Object";
        public FormVid_object(bool meyhod, string id)
        {
            InitializeComponent();
            if (meyhod)
            {
                res = true;
            }
            else { res = false; idput = int.Parse(id); Put(); }
        }
        private async void Put()
        {
            Vid_object client = await ConnectionAPI<Vid_object>.Get(TableName, idput);
            title.Text = client.title.ToString();
        }
        private async void ADD_Click(object sender, EventArgs e)
        {
                if (title.Text != "")
                {
                    JObject jArray = new JObject();
                    jArray.Add("title", title.Text.ToString());
                    bool resaul = false;
                    string json = JsonConvert.SerializeObject(jArray);
                    if (res)
                    {
                         resaul = await ConnectionAPI<Vid_object>.Post(json, TableName);
                    }
                    else 
                    {
                         resaul = await ConnectionAPI<Vid_object>.Put(json, idput, TableName);
                    }
                        
                    if (resaul)
                    {
                        Close();
                    }
                    else { MessageBox.Show("Не удалось вставить запись"); }
                }
                else { MessageBox.Show("Не все данные заполнены"); }
            
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
