﻿using Abstraction.Entity;
using APP.Data;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace APP.Forms
{
    public partial class FormContract : Form
    {
        private bool res = true;
        private int idput = 0;
        private string TableName = "Contract";
        private Dictionary<string, long> clist = new Dictionary<string, long>();
        private Dictionary<string, long> tslist = new Dictionary<string, long>();
        private Dictionary<long, string> vilist1 = new Dictionary<long, string>();
        private Dictionary<long, string> vilist2 = new Dictionary<long, string>();
        public FormContract(bool meyhod, string id)
        {
            InitializeComponent();
            GetClient();
            Gettypeinsur();
            Client_id.DropDownStyle = ComboBoxStyle.DropDownList;
            type_insu.DropDownStyle = ComboBoxStyle.DropDownList;
            if (meyhod) { res = true; }
            else { res = false; idput = int.Parse(id); Put(); }
        }
        private async void Put()
        {
            Contract client = await ConnectionAPI<Contract>.Get(TableName, idput);
            Client_id.Text = vilist1[client.client_id].ToString();
            datestart.Text = client.date_start.ToString();
            dateend.Text = client.date_end.ToString();
            sum_ins.Text = client.sum_insurance.ToString();
            type_insu.Text = vilist2[client.type_Insurance_id].ToString();
            name.Text = client.Name.ToString();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            bool resaul = false;
            if (Client_id.Text != "" & datestart.Text != "" & dateend.Text != "" & sum_ins.Text != "" & type_insu.Text != "")
            {
                if (sum_ins.Value < 0)
                {
                    MessageBox.Show("Сумма не может быть меньше 0");
                }
                else
                {
                    JObject jArray = new JObject();
                    jArray.Add("client_id", int.Parse(clist[Client_id.Text].ToString()));
                    jArray.Add("date_start", datestart.Value.ToString("yyyy-MM-dd"));
                    jArray.Add("date_end", dateend.Value.ToString("yyyy-MM-dd"));
                    jArray.Add("sum_insurance", int.Parse(sum_ins.Text));
                    jArray.Add("type_Insurance_id", int.Parse(tslist[type_insu.Text].ToString()));
                    jArray.Add("name", name.Text);
                    string json = JsonConvert.SerializeObject(jArray);
                    if (res)
                    {
                        resaul = await ConnectionAPI<Contract>.Post(json, TableName);
                    }
                    else
                    {
                        resaul = await ConnectionAPI<Contract>.Put(json, idput, TableName);
                    }
                    if (resaul)
                    {
                        Close();
                    }
                    else { MessageBox.Show("Не удалось вставить запись"); }
                }
            }
            else { MessageBox.Show("Не все данные заполнены"); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
        private async void GetClient()
        {
            List<Client> type_Insurances =  await ConnectionAPI<Client>.Get("Client");
            foreach (Client i in type_Insurances)
            {
                Client_id.Items.Add(i.FIO);
                clist.Add(i.FIO, i.clients_id);
                vilist1.Add(i.clients_id, i.FIO);
            }
        }
        private async void Gettypeinsur()
        {
            List<Type_Insurance> type_Insurances = await ConnectionAPI<Type_Insurance>.Get("Type_Insurance");
            foreach (Type_Insurance i in type_Insurances)
            {
                type_insu.Items.Add(i.title);
                tslist.Add(i.title, i.Type_Insurance_id);
                vilist2.Add(i.Type_Insurance_id, i.title);
            }
        }
        

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void FormContract_Load(object sender, EventArgs e)
        {

        }
    }
}
