﻿namespace APP.Forms
{
    partial class FormVid_object
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Dbl = new System.Windows.Forms.Label();
            this.title = new System.Windows.Forms.TextBox();
            this.ADD = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Dbl
            // 
            this.Dbl.AutoSize = true;
            this.Dbl.Location = new System.Drawing.Point(17, 20);
            this.Dbl.Name = "Dbl";
            this.Dbl.Size = new System.Drawing.Size(59, 15);
            this.Dbl.TabIndex = 1;
            this.Dbl.Text = "Название";
            // 
            // title
            // 
            this.title.Location = new System.Drawing.Point(90, 12);
            this.title.MaxLength = 100;
            this.title.Multiline = true;
            this.title.Name = "title";
            this.title.Size = new System.Drawing.Size(178, 33);
            this.title.TabIndex = 3;
            // 
            // ADD
            // 
            this.ADD.Location = new System.Drawing.Point(12, 51);
            this.ADD.Name = "ADD";
            this.ADD.Size = new System.Drawing.Size(132, 31);
            this.ADD.TabIndex = 4;
            this.ADD.Text = "Добавить/Изменить";
            this.ADD.UseVisualStyleBackColor = true;
            this.ADD.Click += new System.EventHandler(this.ADD_Click);
            // 
            // Cancel
            // 
            this.Cancel.Location = new System.Drawing.Point(196, 51);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(78, 31);
            this.Cancel.TabIndex = 5;
            this.Cancel.Text = "Отмена";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // FormVid_object
            // 
            this.AcceptButton = this.ADD;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 99);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.ADD);
            this.Controls.Add(this.title);
            this.Controls.Add(this.Dbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "FormVid_object";
            this.Text = "Вид объекта";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label Dbl;
        private System.Windows.Forms.TextBox title;
        private System.Windows.Forms.Button ADD;
        private System.Windows.Forms.Button Cancel;
    }
}