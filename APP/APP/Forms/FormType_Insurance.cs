﻿using Abstraction.Entity;
using APP.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace APP.Forms
{
    public partial class FormType_Insurance : Form
    {
        private bool res = true;
        private int idput = 0;
        private string TableName = "Type_Insurance";
        private Dictionary<string, long> vilist = new Dictionary<string, long>();
        private Dictionary<long, string> vilist1 = new Dictionary<long, string>();
        public FormType_Insurance(bool meyhod, string id)
        {
            InitializeComponent();

            Vid_insu_id.DropDownStyle = ComboBoxStyle.DropDownList;
            GetVid_in();
            if (meyhod) { res = true; }
            else { res = false; idput = int.Parse(id); Put(); }
        }
        private async void Put()
        {
            Type_Insurance client = await ConnectionAPI<Type_Insurance>.Get(TableName, idput);
            title.Text = client.title.ToString();
            Vid_insu_id.Text = vilist1[client.Vid_Insurance_id].ToString();
            discriptoin.Text = client.discription.ToString();
        }


        private async void button1_Click(object sender, EventArgs e)
        {
            bool resaul = false;
            if (title.Text != "" & Vid_insu_id.Text != "" & discriptoin.Text != "")
            {
                JObject jArray = new JObject();
                jArray.Add("title", title.Text);
                jArray.Add("vid_Insurance_id", int.Parse(vilist[Vid_insu_id.Text].ToString()));
                jArray.Add("discription", discriptoin.Text);
                string json = JsonConvert.SerializeObject(jArray);
                if (res)
                {
                    resaul = await ConnectionAPI<Type_Insurance>.Post(json, TableName);
                }
                else
                {
                    resaul = await ConnectionAPI<Type_Insurance>.Put(json, idput, TableName);
                }
                if (resaul)
                {
                    Close();
                }
                else { MessageBox.Show("Не удалось вставить запись"); }
            }
            else { MessageBox.Show("Не все данные заполнены"); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
        private async void GetVid_in()
        {
            List<Vid_Insurance> type_Insurances = await ConnectionAPI<Vid_Insurance>.Get("Vid_insurance");
            foreach (Vid_Insurance i in type_Insurances)
            {
                Vid_insu_id.Items.Add(i.title);
                vilist.Add(i.title, i.Vid_Insurance_id);
                vilist1.Add(i.Vid_Insurance_id, i.title);
            }
        }
    }
}
