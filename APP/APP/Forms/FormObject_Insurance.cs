﻿using Abstraction.Entity;
using APP.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace APP.Forms
{
    public partial class FormObject_Insurance : Form
    {
        private bool res = true;
        private int idput = 0;
        private string TableName = "Object_Insurance";
        private Dictionary<string, long> oiist = new Dictionary<string, long>();
        private Dictionary<long, string> vilist1 = new Dictionary<long, string>();
        public FormObject_Insurance(bool meyhod, string id)
        {
            InitializeComponent();
            Vid_object.DropDownStyle = ComboBoxStyle.DropDownList;
            GetVid_in();
            if (meyhod) { res = true; }
            else { res = false; idput = int.Parse(id); Put(); }
        }
        private async void Put()
        {
            Object_Insurance client = await ConnectionAPI<Object_Insurance>.Get(TableName, idput);
            Discription.Text = client.discripton.ToString();
            kef.Text = client.Price.ToString();
            Vid_object.Text = vilist1[client.vid_Object_id].ToString();
        }

        private async void button1_Click(object sender, EventArgs e)
        {
            bool resaul = false;
            if (Discription.Text != "" & kef.Text != "" & Vid_object.Text != "")
            {
                if (kef.Value <= 0)
                {
                    MessageBox.Show("Сумма не может быть меньше 0");
                }
                else
                {
                    JObject jArray = new JObject();
                    jArray.Add("discripton", Discription.Text);
                    jArray.Add("vid_Object_id", int.Parse(oiist[Vid_object.Text].ToString()));
                    jArray.Add("price", int.Parse(kef.Text));
                    string json = JsonConvert.SerializeObject(jArray);
                    if (res)
                    {
                        resaul = await ConnectionAPI<Object_Insurance>.Post(json, TableName);
                    }
                    else
                    {
                        resaul = await ConnectionAPI<Object_Insurance>.Put(json, idput, TableName);
                    }
                    if (resaul)
                    {
                        Close();
                    }
                    else { MessageBox.Show("Не удалось вставить запись"); }
                }
            }
            else { MessageBox.Show("Не все данные заполнены"); }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }
        private async void GetVid_in()
        {
            List<Vid_object> type_Insurances = await ConnectionAPI<Vid_object>.Get("Vid_Object");
            foreach (Vid_object i in type_Insurances)
            {
                Vid_object.Items.Add(i.title);
                oiist.Add(i.title, i.Vid_object_id);
                vilist1.Add(i.Vid_object_id, i.title);
            }
        }
    }
}
