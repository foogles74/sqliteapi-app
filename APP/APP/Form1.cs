﻿using Abstraction.Entity;
using APP.Data;
using RestSharp;
using System;
using System.Drawing;
using System.Net;
using System.Windows.Forms;

namespace APP
{
    public partial class Form_Sign_In : Form
    {
        public Form_Sign_In()
        {
            InitializeComponent();
            if (Properties.Settings.Default.Login != string.Empty)
            {
                textLogin.Text = Properties.Settings.Default.Login;
                textPassword.Text = Properties.Settings.Default.Password;
            }
        }

        private async void OK_Click(object sender, EventArgs e)
        {
            HttpStatusCode restResponse =  ConnectionAPI<Users>.Login(textLogin.Text, textPassword.Text);
            if (restResponse == HttpStatusCode.OK) 
            { 
                Properties.Settings.Default.Login = textLogin.Text;
                Properties.Settings.Default.Password = textPassword.Text;
                Properties.Settings.Default.Save();
                this.DialogResult = DialogResult.OK;
            }
            else if (restResponse == HttpStatusCode.InternalServerError)
            { MessageBox.Show("Не правильный логин или пароль"); }
            else if (restResponse == 0)
            { MessageBox.Show("Серевер не доступен"); }

        }
        private void Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
        private void Mouse_Enter_OK(object sender, EventArgs e)
        {
            OK.ForeColor = Color.Green;
        }
        private void Mouse_Enter_Cancel(object sender, EventArgs e)
        {
            Cancel.ForeColor = Color.Red;
        }
        private void Mouse_Leave_OK(object sender, EventArgs e)
        {
            OK.ForeColor = Color.Black;
        }
        private void Mouse_Leave_Cancel(object sender, EventArgs e)
        {
            Cancel.ForeColor = Color.Black;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }



        private void Form_Sign_In_Load(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
