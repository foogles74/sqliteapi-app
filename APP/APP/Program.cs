using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using APP.Forms;

namespace APP
{
    internal static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Form_Sign_In form_Sign_In = new Form_Sign_In();
            DialogResult result = form_Sign_In.ShowDialog();
            if (result == DialogResult.OK)
            {
                frm_main frm = new frm_main();
                frm.ShowDialog();
            }
            else if (result == DialogResult.Cancel) 
            { Application.Exit(); }

            

        }
    }
}
