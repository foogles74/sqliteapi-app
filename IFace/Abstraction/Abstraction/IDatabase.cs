﻿using System;
using System.Collections.Generic;
using System.Text;
using Abstraction.Database;

namespace Abstraction
{
    public interface IDatabase
    {
        public IDatabaseClient Client { get; }
        public IDatabaseContract Contract { get; }
        public IDatabaseObject_Insurance Object_Insurance { get; }
        public IDatabaseDocument Document { get; }
        public IDatabaseObject_vid Object_Vid { get; }
        public IDatabaseType_insurance Type_Insurance { get; }
        public IDatabaseVid_Insurance Vid_Insurance { get; }
        public IDatabaseVid_object Vid_Object { get; }
        public IDatabaseUsers Users { get; }

    }
}
