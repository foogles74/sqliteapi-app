﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public class Object_vid : IObject_vid
    {
        public const string PK = "Object_vid_id";
        public long object_vid_id { get; set; }
        public long vid_insurance_id { get; set; }
        public long vid_object_id { get; set; }
    }
}
