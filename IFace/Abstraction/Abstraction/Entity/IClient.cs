﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IClient
    {
        public long clients_id { get; set; }
        public string phone { get; set; }
        public string pasport { get; set; }
        public string polise { get; set; }
        public string FIO   { get; set; }
    }
}
