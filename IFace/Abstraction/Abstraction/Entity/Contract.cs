﻿using System;

namespace Abstraction.Entity
{
    public class Contract : IContract
    {
        public const string PK = "Contract_id";
        public long contract_id { get; set; }
        public long client_id { get; set; }
        public DateTime date_start { get; set; }
        public DateTime date_end { get; set; }
        public double sum_insurance { get; set; }
        public long type_Insurance_id { get; set; }
        public string Name { get; set; }
    }
}
