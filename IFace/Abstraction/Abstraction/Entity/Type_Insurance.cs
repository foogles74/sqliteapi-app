﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public class Type_Insurance : IType_Insurance
    {
        public const string PK = "Type_Insurance_id";
        public long Type_Insurance_id { get; set; }
        public string title { get; set; }
        public long Vid_Insurance_id { get; set; }
        public string discription { get; set; }
    }
}
