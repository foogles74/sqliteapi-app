﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public class Document : IDocument
    {
        public const string PK = "Document_id";
        public long document_id { get; set; }
        public long contract_id { get; set; }
        public long object_Insurance_id { get; set; }
    }
}
