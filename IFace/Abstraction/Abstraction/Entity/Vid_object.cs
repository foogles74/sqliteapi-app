﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public class Vid_object : IVid_object
    {
        public long Vid_object_id { get; set; }
        public string title { get; set; }
        public const string PK = "Vid_object_id";
    }
}
