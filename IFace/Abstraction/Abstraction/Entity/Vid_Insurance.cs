﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public class Vid_Insurance : IVid_Insurance
    {
        public const string PK = "Vid_Insurance_id";
        public long Vid_Insurance_id { get; set; }
        public string title { get; set; }
    }
}
