﻿using System;
using System.Collections.Generic;
using System.Text;
#nullable enable
namespace Abstraction.Entity
{
    public interface IVid_Insurance
    {
        public long Vid_Insurance_id { get; set; }
        public string title { get; set; }
    }
}
