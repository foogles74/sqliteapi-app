﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IDocument
    {
        public long document_id { get; set; }
        public long contract_id { get; set; }
        public long object_Insurance_id { get; set; }
    }
}
