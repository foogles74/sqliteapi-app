﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IObject_vid
    {
        public long object_vid_id { get; set; }
        public long vid_insurance_id { get; set; }
        public long vid_object_id { get; set; }
    }
}
