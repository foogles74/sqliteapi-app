﻿namespace Abstraction.Entity
{
    public class Client : IClient
    {
        public const string PK = "Clients_id";
        public long clients_id { get; set; }
        public string phone { get; set; }
        public string pasport { get; set; }
        public string polise { get; set; }
        public string FIO { get; set; }
    }
}
