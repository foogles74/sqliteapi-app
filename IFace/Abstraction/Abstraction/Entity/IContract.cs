﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstraction.Entity
{
    public interface IContract
    {
        public long contract_id { get; set; }
        public long client_id { get; set; }
        public DateTime date_start { get; set; }
        public DateTime date_end { get; set; }
        public double sum_insurance { get; set; }
        public long type_Insurance_id { get; set; }
        public string Name { get; set; }
    }
}
